﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicSolution;

namespace MesozoicTest
{
    [TestClass]
    public class Dinosaure
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaure louis = new Dinosaure("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.name);
            Assert.AreEqual("Stegausaurus", louis.specie);
            Assert.AreEqual(12, louis.age);
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaure louis = new Dinosaure("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaure louis = new Dinosaure("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
    }
}
