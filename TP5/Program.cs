﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicConsole
{
   public class Program
    {
        static void Main(string[] args)
        {
            Mesozoic.Dinosaur louis = new Mesozoic.Dinosaur("Louis", "Stegausaurus", 12);
            Mesozoic.Dinosaur nessie = new Mesozoic.Dinosaur("Nessie", "Diplodocus", 11);
            Mesozoic.Dinosaur Jean = new Mesozoic.Dinosaur("Jean", "Stegausaurus", 25);
            Mesozoic.Dinosaur Neymar = new Mesozoic.Dinosaur("Neymar", "Diplodocus", 46);


            List<Mesozoic.Dinosaur> dinosaurs = new List<Mesozoic.Dinosaur>();

            dinosaurs.Add(louis); //Append dinosaur reference to end of list
            dinosaurs.Add(nessie);
            dinosaurs.Add(Jean);
            dinosaurs.Add(Neymar);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Mesozoic.Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.name);
            }

            dinosaurs.RemoveAt(1); //Remove dinosaur at index 1

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Mesozoic.Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.name);
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Mesozoic.Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.name);
            }
            Console.ReadKey();
        }
    }
}
